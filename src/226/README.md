Problem 226
===========


   The blancmange curve is the set of points (x,y) such that 0 ≤ x ≤ 1 and ,
   where s(x) = the distance from x to the nearest integer.

   The area under the blancmange curve is equal to ½, shown in pink in the
   diagram below.

                              [1]blancmange curve

   Let C be the circle with centre (¼,½) and radius ¼, shown in black in the
   diagram.

   What area under the blancmange curve is enclosed by C?
   Give your answer rounded to eight decimal places in the form 0.abcdefgh


   Visible links
   p_226_formula.gif
   p_226_scoop2.gif
   Answer: ce6fd32d1d2fb58c4c0c1f7962c39f04


