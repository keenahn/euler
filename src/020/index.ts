import { sum } from 'lodash/fp'

import { fact, stringToIntArray } from '../helpers'

// Find the sum of the digits in the number 100!
const fn = () => {
  return sum(stringToIntArray(`${fact(100)}`))
}

export { fn }
