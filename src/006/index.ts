import { map, sum } from 'lodash/fp'

import { range, sq } from '../helpers'

const fn = () => {
  const a = sum(map(sq, range(100)))
  const b = sq(sum(range(100)))
  return b - a
}

export { fn }
