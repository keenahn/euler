Problem 291
===========


   A prime number p is called a Panaitopol prime if for some positive
   integers
   x and y.

   Find how many Panaitopol primes are less than 5×10^15.


   p_291_formula.gif
   Answer: 15d4b4d97452ca7d219e3fa72f6b7aef


