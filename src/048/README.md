Problem 48
==========


   The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.

   Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.


   Answer: 0829124724747ae1c65da8cae5263346


