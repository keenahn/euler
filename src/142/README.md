Problem 142
===========


   Find the smallest x + y + z with integers x > y > z > 0 such that x + y, x
   − y, x + z, x − z, y + z, y − z are all perfect squares.


   Answer: d3de282705508407532aa20ca8928e3b


