Problem 279
===========


   How many triangles are there with integral sides, at least one integral
   angle (measured in degrees), and a perimeter that does not exceed 10^8?


   Answer: 1f51455a8180fdeeb21285dfb6cba45f


