Problem 81
==========


   In the 5 by 5 matrix below, the minimal path sum from the top left to the
   bottom right, by only moving to the right and down, is indicated in bold
   red and is equal to 2427.

                              131 673 234 103 18
                              201 96  342 965 150
                              630 803 746 422 111
                              537 699 497 121 956
                              805 732 524 37  331

   Find the minimal path sum, in [1]matrix.txt, a 31K text file containing a
   80 by 80 matrix, from the top left to the bottom right by only moving
   right and down.


   Visible links
   1. matrix.txt
   Answer: f9ffec84499832add77e6a8bb00246ec


