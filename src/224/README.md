Problem 224
===========


   Let us call an integer sided triangle with sides a ≤ b ≤ c barely obtuse
   if the sides satisfy
   a^2 + b^2 = c^2 - 1.

   How many barely obtuse triangles are there with perimeter ≤ 75,000,000?


   Answer: c43cfb12750dee27b4b0d016261e831b


