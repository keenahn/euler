Problem 290
===========


   How many integers 0 ≤ n < 10^18 have the property that the sum of the
   digits of n equals the sum of digits of 137n?


   Answer: 8246684fec8ece9f0ee3c9898c8c9d6a


