Problem 85
==========


   By counting carefully it can be seen that a rectangular grid measuring 3
   by 2 contains eighteen rectangles:

   Although there exists no rectangular grid that contains exactly two
   million rectangles, find the area of the grid with the nearest solution.


   p_085.gif
   Answer: 92bf5e6240737e0326ea59846a83e076


