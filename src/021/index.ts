import { filter, sum, uniq } from 'lodash/fp'

import { factors, range } from '../helpers'

const sumDivisors = (n: number) => sum(factors(n))

const cache: { [k: number]: number } = {}

// Evaluate the sum of all the amicable numbers under 10000.
const fn = () => {
  for (let i = 1; i <= 9999; i++) {
    cache[i] = sumDivisors(i) - i
  }

  const isAmicable = (n: number) => n === cache[cache[n]] && n !== cache[n]

  const amicable = uniq(filter(isAmicable, range(9999)))

  return sum(amicable)
}

export { fn }
