import { sum } from 'lodash/fp'

// What is the sum of the digits of the number 2^1000?

// Feels cheaty to use BigInt, might want to revisit without it
const fn = () => {
  return sum(
    BigInt(Math.pow(2, 1000))
      .toString()
      .split('')
      .map((n) => parseInt(n, 10))
  )
}

export { fn }
