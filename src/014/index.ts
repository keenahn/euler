const seqLengthCache: { [k: number]: number } = {}

const memoizedSeqLength = (n: number) => {
  if (n === 1) return 1
  if (seqLengthCache[n]) return seqLengthCache[n]

  seqLengthCache[n] = 1 + memoizedSeqLength(n % 2 === 0 ? n / 2 : 3 * n + 1)
  return seqLengthCache[n]
}

const fn = () => {
  let currMaxLength = 0
  let currMax = 1
  for (let i = 1; i < 1000000; i++) {
    const curr = memoizedSeqLength(i)
    if (curr > currMaxLength) {
      currMaxLength = curr
      currMax = i
    }
  }

  return currMax
}

export { fn }
