Problem 41
==========


   We shall say that an n-digit number is pandigital if it makes use of all
   the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital
   and is also prime.

   What is the largest n-digit pandigital prime that exists?


   Answer: d0a1bd6ab4229b2d0754be8923431404


