import { reduce, times } from 'lodash/fp'

// Find the sum of all the multiples of 3 or 5 below 1000.
const fn = () =>
  reduce((acc: number, next: number) => acc + next)(0)(
    times((n: number) => {
      if (n % 3 === 0 || n % 5 === 0) return n
      return 0
    })(1000)
  )

export { fn }
