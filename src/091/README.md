Problem 91
==========


   The points P (x[1], y[1]) and Q (x[2], y[2]) are plotted at integer
   co-ordinates and are joined to the origin, O(0,0), to form ΔOPQ.

   There are exactly fourteen triangles containing a right angle that can be
   formed when each co-ordinate lies between 0 and 2 inclusive; that is,
   0 ≤ x[1], y[1], x[2], y[2] ≤ 2.

   Given that 0 ≤ x[1], y[1], x[2], y[2] ≤ 50, how many right triangles can
   be formed?


   p_091_1.gif
   p_091_2.gif
   Answer: e8dc153260a59d4f236cfd7439d5dfd3


