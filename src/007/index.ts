import { sieve } from '../helpers'
// What is the 10,001st prime number?
// Answer: 8c32ab09ec0210af60d392e9b2009560

const fn = () => {
  const primes = sieve(1000000)
  return primes[10000]
}

export { fn }
