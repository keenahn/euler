// Find the largest palindrome made from the product of two 3-digit numbers.
// Answer: d4cfc27d16ea72a96b83d9bdef6ce2ec

const isPalindrome = (n: number) => {
  const s = `${n}`
  const rev = s.split('').reverse().join('')
  return s == rev
}

const fn = () => {
  let curMax = 0

  const products: {
    [k: number]: {
      [k2: number]: boolean
    }
  } = {}

  for (let i = 100; i < 1000; i++) {
    for (let j = 100; j < 1000; j++) {
      // This check cuts the time down by about half, at the cost of more memory
      if (products[i]?.[j] || products[j]?.[i]) continue

      const curr = i * j
      if (isPalindrome(curr) && curr > curMax) curMax = curr
      if (i < j) {
        if (!products[i]) {
          products[i] = { [j]: true }
        } else {
          products[i][j] = true
        }
      } else {
        if (!products[j]) {
          products[j] = { [i]: true }
        } else {
          products[j][i] = true
        }
      }
    }
  }
  return curMax
}

export { fn }
