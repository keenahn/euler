Problem 206
===========


   Find the unique positive integer whose square has the form
   1_2_3_4_5_6_7_8_9_0,
   where each “_” is a single digit.


   Answer: 09f9d87cb4b1ebb34e1f607e55a351d8


