import { padStart, trimStart } from 'lodash'
import { reduce, tail } from 'lodash/fp'

// Return all prime numbers less than or equal to n
const sieve = (n: number) => {
  const arr = new Array(n + 1)
  let i = 2

  while (i < n) {
    if (arr[i] === undefined) {
      arr[i] = true

      // Slight optimization here to start at i squared
      let cur = i * i
      while (cur < n) {
        arr[cur] = false
        cur += i
      }
    }
    i++
  }

  const ret = []
  for (let j = 0; j < n + 1; j++) {
    if (arr[j]) ret.push(j)
  }

  return ret
}

const product = (arr: Array<number>) => reduce((acc: number, next: number) => acc * next, 1, arr)

// From 1 to n
const range = (n: number, zeroBased = false) =>
  zeroBased ? [...Array(n).keys()] : tail([...Array(n + 1).keys()])
const sq = (n: number) => n * n

// First from 015
// Feels cheaty to use BigInt, so perhaps I will rewrite it
const fact = (n: number): bigint => {
  if (n === 1) return BigInt(1)
  return BigInt(n) * fact(n - 1)
}

const stringToIntArray = (s: string) => s.split('').map((n) => parseInt(n, 10))

// First from 013
const sumBigNums = (a: string, b: string) => {
  const bb = a.length > b.length ? padStart(b, a.length, '0') : b
  const aa = b.length > a.length ? padStart(a, b.length, '0') : a

  const arrA = stringToIntArray(aa)
  const arrB = stringToIntArray(bb)

  let carry = 0

  const ret = Array(arrA.length + 1)

  for (let i = arrA.length - 1; i >= 0; i--) {
    let c = arrA[i] + arrB[i] + carry

    if (c >= 10) {
      carry = 1
      c -= 10
    } else {
      carry = 0
    }
    ret[i + 1] = c
  }

  if (carry > 0) {
    ret[0] = carry
  }

  return trimStart(ret.join(''), '0')
}

// First from 012
// Naiive implementation
// Results are not sorted
const factors = (n: number): Array<number> => {
  const ret = [1, n]

  for (let i = 2; i <= Math.sqrt(n); i++) {
    const d = n / i
    if (Number.isInteger(d)) {
      ret.push(d)
      ret.push(i)
    }
  }
  return ret
}

export { product, sieve, range, sq, fact, sumBigNums, stringToIntArray, factors }
