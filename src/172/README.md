Problem 172
===========


   How many 18-digit numbers n (without leading zeros) are there such that no
   digit occurs more than three times in n?


   Answer: f5f260ee21ead7478403c2ccd18a1829


