// Starting in the top left corner of a 2×2 grid, and only being able to move
// to the right and down, there are exactly 6 routes to the bottom right
// corner.

// How many such routes are there through a 20×20 grid?

const fact = (n: number): bigint => {
  if (n === 1) return BigInt(1)
  return BigInt(n) * fact(n - 1)
}

const numPathsForGrid = (n: number) => {
  if (n % 2 === 0) {
    return fact(n) / (fact(n / 2) * fact(n / 2))
  }
  return fact(n) / (fact(Math.floor(n / 2)) * fact(n - Math.floor(n / 2)))
}

const fn = () => {
  // At each step of the way (unless you are on the rightmost or bottommost edge) you can choose to move right or down
  // Every path from the top left to the bottom right in a n x n grid will require n moves right and n moves down
  // let's call a move right r and a move down d
  // Therefore, the number of unique paths will be the unique permutations of
  // rrrrrrrrrrrrrrrrrrrrdddddddddddddddddddd

  // If we had an arbitrary string of 40 unique characters, there would be 40! permutations
  // Since we have 40 characters, but two are repeated 20 times each, we have 40!/(20!*20!)
  // This calculation is beyond the range of ints, so we'll use bigints

  return numPathsForGrid(40)
}

export { fn }
