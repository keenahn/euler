import { filter, sum } from 'lodash/fp'

// Find the sum of all even fib numbers < 4000000

const fibs = [1, 2]

let i = 2
const fn = () => {
  let cur = fibs[i - 1] + fibs[i - 2]
  while (cur < 4000000) {
    fibs[i] = cur
    i++
    cur = fibs[i - 1] + fibs[i - 2]
  }

  return sum(filter((n: number) => n % 2 === 0)(fibs))
}

export { fn }
