// a^2 + b^2 = c^2
// There exists exactly one Pythagorean triplet for which a + b + c = 1000.
// Find the product abc.

import { sq } from '../helpers'

const fn = () => {
  const sums: {
    [k: number]: {
      [k2: number]: boolean
    }
  } = {}

  for (let i = 1; i < 1000; i++) {
    for (let j = 1; j < 1000; j++) {
      if (sums[i]?.[j] || sums[j]?.[i]) continue

      const curr = sq(i) + sq(j)
      const c = Math.sqrt(curr)

      if (Number.isInteger(c) && i + j + c === 1000) {
        return i * j * c
      }

      if (i < j) {
        if (!sums[i]) {
          sums[i] = { [j]: true }
        } else {
          sums[i][j] = true
        }
      } else {
        if (!sums[j]) {
          sums[j] = { [i]: true }
        } else {
          sums[j][i] = true
        }
      }
    }
  }
  return
}

export { fn }
