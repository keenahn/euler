Problem 454
===========


   In the following equation x, y, and n are positive integers.

                                   1   1   1
                                   ─ + ─ = ─
                                   x   y   n

   For a limit L we define F(L) as the number of solutions which satisfy x <
   y ≤ L.

   We can verify that F(15) = 4 and F(1000) = 1069.
   Find F(10^12).


   Answer: cf4e45f50c511e558b3dccb3ed481cb5


