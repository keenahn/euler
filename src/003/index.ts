import { sieve } from '../helpers'
// What is the largest prime factor of the number 600851475143 ?
// Answer: 94c4dd41f9dddce696557d3717d98d82

const fn = () => {
  let target = 600851475143
  const primes = sieve(1000000)
  let i = 0
  let curMax = primes[0]
  while (i < primes.length) {
    const cur = primes[i]
    while (target % cur === 0) {
      curMax = cur
      target = target / cur
    }
    i++
  }
  return curMax
}

export { fn }
