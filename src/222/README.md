Problem 222
===========


   What is the length of the shortest pipe, of internal radius 50mm, that can
   fully contain 21 balls of radii 30mm, 31mm, ..., 50mm?

   Give your answer in micrometres (10^-6 m) rounded to the nearest integer.


   Answer: 6984ba429b968467619ec98a8ee51abf


