import { map, sum } from 'lodash/fp'

import { range } from '../helpers'

// If the numbers 1 to 5 are written out in words: one, two, three, four,
// five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

// If all the numbers from 1 to 1000 (one thousand) inclusive were written
// out in words, how many letters would be used?

// NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
// forty-two) contains 23 letters and 115 (one hundred and fifteen) contains
// 20 letters. The use of "and" when writing out numbers is in compliance
// with British usage.

// Answer: 6a979d4a9cf85135408529edc8a133d0

const basics: { [k: number]: string } = {
  1: 'one',
  2: 'two',
  3: 'three',
  4: 'four',
  5: 'five',
  6: 'six',
  7: 'seven',
  8: 'eight',
  9: 'nine',
  10: 'ten',
  11: 'eleven',
  12: 'twelve',
  13: 'thirteen',
  14: 'fourteen',
  15: 'fifteen',
  16: 'sixteen',
  17: 'seventeen',
  18: 'eighteen',
  19: 'nineteen',
} as const

const multiplesOfTenToWord: { [k: number]: string } = {
  20: 'twenty',
  30: 'thirty',
  40: 'forty',
  50: 'fifty',
  60: 'sixty',
  70: 'seventy',
  80: 'eighty',
  90: 'ninety',
}

const numToWord = (n: number): string => {
  if (n < 20) {
    return basics[n]
  }
  if (n < 100) {
    const tens = Math.floor(n / 10) * 10
    const ones = n - tens
    if (ones > 0) return `${multiplesOfTenToWord[tens]}-${basics[ones]}`
    return multiplesOfTenToWord[tens]
  }
  if (n < 1000) {
    const hundreds = Math.floor(n / 100) * 100
    const rest = n - hundreds
    if (rest > 0) return `${basics[hundreds / 100]} hundred and ${numToWord(rest)}`
    return `${basics[hundreds / 100]} hundred`
  }
  if (n === 1000) {
    return 'one thousand'
  }
  return 'unknown'
}

const replaceNonAlpha = (s: string) => s.replace(/\-/g, '').replace(/ /g, '')

const fn = () => sum(map((n: number) => replaceNonAlpha(numToWord(n)).length, range(1000)))

export { fn }
