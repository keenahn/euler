Problem 164
===========


   How many 20 digit numbers n (without any leading zero) exist such that no
   three consecutive digits of n have a sum greater than 9?


   Answer: 6e96debf3bfe7cc132401bafe5a5d6d6


