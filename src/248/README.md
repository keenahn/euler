Problem 248
===========


   The first number n for which φ(n)=13! is 6227180929.

   Find the 150,000^th such number.


   Answer: b69a3ba674f6c7c5f2ce244f9e9cc873


