import { expect } from 'chai'
import { Md5 } from 'ts-md5/dist/md5'

import { fn } from '../../../src/016'

describe('016', () => {
  it('should work', async () => {
    const actual = fn()
    expect(Md5.hashStr(`${actual}`)).to.equal('6a5889bb0190d0211a991f47bb19a777')
  })
})
