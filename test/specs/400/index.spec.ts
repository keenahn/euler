import { expect } from 'chai'
import { Md5 } from 'ts-md5/dist/md5'

import { fn } from '../../../src/001'

describe('001', () => {
  xit('should work', async () => {
    const actual = fn()
    expect(Md5.hashStr(`${actual}`)).to.equal('e1edf9d1967ca96767dcc2b2d6df69f4')
  })
})
