import { expect } from 'chai'
import { Md5 } from 'ts-md5/dist/md5'

import { fn } from '../../../src/012'

describe('012', () => {
  it('should work', async () => {
    const actual = fn()
    expect(Md5.hashStr(`${actual}`)).to.equal('8091de7d285989bbfa9a2f9f3bdcc7c0')
  })
})
