import { expect } from 'chai'
import { Md5 } from 'ts-md5/dist/md5'

import { fn } from '../../../src/014'

describe('014', () => {
  it('should work', async () => {
    const actual = fn()
    expect(Md5.hashStr(`${actual}`)).to.equal('5052c3765262bb2c6be537abd60b305e')
  })
})
