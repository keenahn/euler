import { expect } from 'chai'
import { Md5 } from 'ts-md5/dist/md5'

import { fn } from '../../../src/013'

describe('013', () => {
  it('should work', async () => {
    const actual = fn()
    expect(Md5.hashStr(`${actual}`)).to.equal('361113f19fd302adc31268f8283a4f2d')
  })
})
