import { expect } from 'chai'
import { Md5 } from 'ts-md5/dist/md5'

import { fn } from '../../../src/006'

describe('006', () => {
  it('should work', async () => {
    const actual = fn()
    expect(Md5.hashStr(`${actual}`)).to.equal('867380888952c39a131fe1d832246ecc')
  })
})
