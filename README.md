# Project Euler

## Personal Rules
- I will allow myself to use `lodash/fp` functions for convenience's sake, as I don't want to have to write `map`, `reduce`, `filter`, `find` etc.
- I will allow myself to reuse math functions that I have written for previous questions
- My first pass at a question will be just to rush to get the answer, optimizing for developer time. I'll only focus on performance if a dumb method literally can't run on my laptop in a reasonable time. Once I finish a bunch of problems, I'll go back and see how to optimize them.

